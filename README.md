#Birt Plugin - Bar In Codes

This is a free and open source eclipse birt plugin project to allow you to use Codebars inside Birt Reports.

Este projeto � um plugin Birt gratuito e open source para permitir a impress�o de c�digo de barras nos Relat�rio do BIRT.

#Inspiring

As I couldn't find any free birt plugin barcode to use inside birt reports, I decide to build my own and help the community :) 

	Enjoy
	
Como n�o encontrei nenhum plugin birt de c�digo de barras gratuito e aberto, decidi criar o meu e ajudar a comunidade open source :) 

	Se divirta!


#Getting Started 

At this point it's at very beginning and not even functional. Hold on for more updates. Feel free to contribute and suggests for updates/features.

Neste momento est� em um est�gio inicial, n�o est� funcional. Sinta-se livre para contribuir ou sugerir melhorias/funcionalidades. 

#Authors
* **Daniel Silva** - *Initial Work*

#Sources 
The sites that I researched to get the things done: 

	http://www.vineetmanohar.com/2010/09/java-barcode-api/
	http://www.eclipse.org/articles/Article-BIRT-ExtensionTutorial1/index.html
	http://www.eclipse.org/articles/article.php?file=Article-BIRT-ExtensionTutorial2/index.html
	http://www.java2s.com/Code/Java/SWT-JFace-Eclipse/CatalogSWT-JFace-Eclipse.htm