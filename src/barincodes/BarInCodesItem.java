package barincodes;

import java.util.List;
import java.util.function.Consumer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.extension.ReportItem;
import org.eclipse.birt.report.model.api.metadata.IChoice;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

public class BarInCodesItem extends ReportItem {

	public static final String EXTENSION_NAME = "BarInCodes";
	public static final String CONTENT_PROP = "content"; 
	public static final String TYPE_PROP = "type";
	public static final String ROTATION_ANGLE_PROP = "rotationAngle";
	public static final String WIDTH_PROP = "width";
	public static final String HEIGHT_PROP = "height";
	
	private ExtendedItemHandle modelHandle;

	public BarInCodesItem( ExtendedItemHandle modelHandle )
	{
		this.modelHandle = modelHandle;
	}
	
	public ExtendedItemHandle getModelHandle( )
	{
		return modelHandle;
	}

	public String getContent( )
	{
		return modelHandle.getStringProperty( CONTENT_PROP );
	}

	public String getType( )
	{
		return modelHandle.getStringProperty( TYPE_PROP );
	}
	
	public int getRotationAngle( )
	{
		return modelHandle.getIntProperty( ROTATION_ANGLE_PROP );
	}

	public int getWidth() 
	{
		return modelHandle.getIntProperty(WIDTH_PROP);
	}
	
	public int getHeight()
	{
		return modelHandle.getIntProperty(HEIGHT_PROP);
	}
	
	public void setContent( String value ) throws SemanticException
	{
		modelHandle.setProperty( CONTENT_PROP, value );
	}
	
	public List<IChoice> getSelectionList() 
	{
		List<IChoice> selectionList = Arrays.asList(modelHandle.getChoices(TYPE_PROP));
		return selectionList;
	}
	
	public Combo getComboSelectList(Composite contentpane)
	{
		final Combo combo = new Combo(contentpane,SWT.DROP_DOWN);
		List<IChoice> contents = this.getSelectionList();
		contents.forEach(new Consumer<IChoice>() {
			@Override
			public void accept(IChoice item) {
				String text = item.getDisplayName().toString();
				combo.add(text);
			}
		});
		return combo;
	}

	public void setType( String value ) throws SemanticException
	{
		modelHandle.setProperty(TYPE_PROP, value );
	}
	
	public void setRotationAngle( int value ) throws SemanticException
	{
		modelHandle.setProperty( ROTATION_ANGLE_PROP, value );
	}
	
	public void setWidth( int value ) throws SemanticException
	{
		modelHandle.setProperty( WIDTH_PROP, value );
	}
	
	public void setHeight( int value ) throws SemanticException
	{
		modelHandle.setProperty( HEIGHT_PROP, value );
	}
}
