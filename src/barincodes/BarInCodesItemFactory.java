package barincodes;

import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.extension.IMessages;
import org.eclipse.birt.report.model.api.extension.IReportItem;
import org.eclipse.birt.report.model.api.extension.ReportItemFactory;

public class BarInCodesItemFactory extends ReportItemFactory{

	public IReportItem newReportItem( DesignElementHandle modelHandle )
	{
		return new BarInCodesItem((ExtendedItemHandle) modelHandle);
	}

	public IMessages getMessages( )
	{
		// TODO implement this to support localization
		return null;
	}
}
