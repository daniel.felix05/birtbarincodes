package barincodes;

import org.eclipse.birt.report.designer.ui.extensions.IReportItemLabelProvider;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.extension.ExtendedElementException;
import org.eclipse.birt.report.model.api.extension.IReportItem;

public class BarInCodesLabelUI implements IReportItemLabelProvider {
	
	public String getLabel( ExtendedItemHandle handle )
	{
		try
		{
			IReportItem item = handle.getReportItem( );
			if ( item instanceof BarInCodesItem )
			{
				return ( (BarInCodesItem) item ).getContent();
			}
		}
		catch ( ExtendedElementException e )
		{
			e.printStackTrace( );
		}
		return null;
	}
	

}
