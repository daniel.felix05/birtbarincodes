package barincodes.engine;


import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.report.engine.extension.IRowSet;
import org.eclipse.birt.report.engine.extension.ReportItemPresentationBase;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.extension.ExtendedElementException;

//import com.google.zxing.BarcodeFormat;
//import com.google.zxing.WriterException;
//import com.google.zxing.client.j2se.MatrixToImageWriter;
//import com.google.zxing.common.BitMatrix;
//import com.google.zxing.qrcode.QRCodeWriter;

import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import barincodes.BarInCodesItem;
import barincodes.generators.Codebar39Generator;
import barincodes.generators.CodebarGeneratorInterface;

import java.io.File;
import java.io.FileOutputStream;

public class BarInCodesPresentationImpl extends ReportItemPresentationBase {

	private BarInCodesItem item;

	public void setModelObject( ExtendedItemHandle modelHandle )
	{
		try
		{
			item = (BarInCodesItem) modelHandle.getReportItem( );
		}
		catch ( ExtendedElementException e )
		{
			e.printStackTrace( );
		}
	}

	public int getOutputType( )
	{
		return OUTPUT_AS_IMAGE;
	}

	public Object onRowSets(IRowSet[] rowSets) throws BirtException {
		
		if (item == null) {
			return null;
		}
		
		
		ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
		byte[] byteVar = null;
		
		try {

			String type = item.getType();
			String text = item.getContent();

			int width = item.getWidth();
			int height = item.getHeight(); // change the height and width as per your requirement

			int dpi = 150;
			String codebarType = "1";
			CodebarGeneratorInterface codebar = null; //new CodeBar39Generator(dpi);
			
			switch (codebarType)
			{
				case "BAR39": codebar = new Codebar39Generator(dpi);
				// case 2 : QRCODE
				// case 3 .... and so on 
				default: codebar = new Codebar39Generator(dpi);
					
			}
									
					
			ImageIO.write(codebar.generateCodeBar(text), "jpg", byteOutput);
			
			byteVar = byteOutput.toByteArray();
			
			codebar.closeFiles();
			byteOutput.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return byteVar;

	}
	
	
	
	
	
}
