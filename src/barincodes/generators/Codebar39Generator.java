package barincodes.generators;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

public class Codebar39Generator extends CodebarGenerator{

	public Codebar39Generator(int dpi) {
		super(dpi);
	}

	@Override
	public BufferedImage generateCodeBar(String content) throws IOException {
		
		Code39Bean bean = new Code39Bean();			
		
		// Configure the barcode generator
		bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); // makes the narrow bar width exactly one pixel
		bean.setWideFactor(3);
		bean.doQuietZone(false);
	    
	    // Set up the canvas provider for monochrome JPEG output
		BitmapCanvasProvider canvas = new BitmapCanvasProvider(outputStream, "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
		
		// Generate the barcode
		bean.generateBarcode(canvas, content);
		
		// Signal end of generation
		canvas.finish();
		
		return canvas.getBufferedImage();
		
	}

}
