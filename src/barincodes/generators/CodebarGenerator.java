package barincodes.generators;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

abstract class CodebarGenerator implements CodebarGeneratorInterface {

	protected BitmapCanvasProvider canvas;
	protected File outputFile;
	protected OutputStream outputStream;
	protected int dpi;
	
	public CodebarGenerator(int dpi)
	{
		outputFile = new File("out.jpg");
		try {
			outputStream = new FileOutputStream(outputFile);
			this.setCanvas(dpi);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void setCanvas(int dpi) {
		this.dpi = dpi;
		canvas = new BitmapCanvasProvider(outputStream, "image/jpeg", this.dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
	}
	
	public BitmapCanvasProvider getCanvas() {
		return canvas;
	}
	
	
	public void setFile(File file) throws FileNotFoundException 
	{
		outputFile = file;
		outputStream = new FileOutputStream(outputFile);
		this.setCanvas(this.dpi);
	}
	
	public void setFile(File file, int dpi) throws FileNotFoundException 
	{
		outputFile = file;
		outputStream = new FileOutputStream(outputFile);
		this.dpi = dpi;
		this.setCanvas(this.dpi);
	}
	
	public File getFile()
	{
		return outputFile;
	}
	
	public OutputStream getOutPutStream()
	{
		return outputStream;
	}
	
	public void setDpi(int dpi)
	{
		this.dpi = dpi;
	}	
	
	public int getDpi()
	{
		return this.dpi;
	}
	
	public abstract BufferedImage generateCodeBar(String content)  throws IOException;
	
	public void closeFiles()
	{
		try {
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
