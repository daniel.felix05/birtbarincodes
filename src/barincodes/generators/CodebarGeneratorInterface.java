package barincodes.generators;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface CodebarGeneratorInterface {
	
	public abstract BufferedImage generateCodeBar(String content)  throws IOException;
	
	public void closeFiles();
}
