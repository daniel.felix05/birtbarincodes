package views;

import java.util.List;

import org.eclipse.birt.report.designer.ui.views.attributes.AbstractPageGenerator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.birt.report.designer.internal.ui.views.attributes.section.ComboSection;

import barincodes.BarInCodesItem;

public class BarInCodesCustomPage extends BarInCodesGeneralPage {

	private Label lbText, lbAngle;
	private ComboSection cbBarType;

	public void buildUI( Composite parent )
	{
		if ( toolkit == null )
		{
			toolkit = new FormToolkit( Display.getCurrent( ) );
			toolkit.setBorderStyle( SWT.NULL );
		}

		Control[] children = parent.getChildren( );

		if ( children != null && children.length > 0 )
		{
			contentpane = (Composite) children[children.length - 1];

			GridLayout layout = new GridLayout( 2, false );
			layout.marginTop = 8;
			layout.marginLeft = 8;
			layout.verticalSpacing = 12;
			contentpane.setLayout( layout );

			toolkit.createLabel( contentpane, "Content:" ); //$NON-NLS-1$
			lbText = toolkit.createLabel( contentpane, "" ); //$NON-NLS-1$
			GridData gd = new GridData( );
			gd.widthHint = 200;
			lbText.setLayoutData( gd );
			
			//this.cbBarType = createComboSection("type", , "codeType", this.BARCODEITEMNAME);
		    

			toolkit.createLabel(contentpane , "CodeBarType:" ); //$NON-NLS-1$
			lbAngle = toolkit.createLabel( contentpane, "" ); //$NON-NLS-1$
			gd = new GridData( );
			gd.widthHint = 200;
			lbAngle.setLayoutData( gd );

		}
	}

	protected void updateUI( )
	{
		BarInCodesItem item = getItem( );

		if ( item != null )
		{
			String text = item.getContent( );
			lbText.setText( text == null ? "" : text ); //$NON-NLS-1$

			lbAngle.setText( String.valueOf( item.getType() ) );
		}
	}
}
