package views;

import java.util.List;

import org.eclipse.birt.report.designer.ui.dialogs.ExpressionBuilder;
import org.eclipse.birt.report.designer.ui.dialogs.ExpressionProvider;
import org.eclipse.birt.report.designer.ui.views.attributes.AttributesUtil;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import barincodes.BarInCodesItem;


public class BarInCodesGeneralPage extends AttributesUtil.PageWrapper {
	protected FormToolkit toolkit;
	protected Object input;
	protected Composite contentpane;
	protected Combo cbCodeBarTypeList;

	private Text txtText, txtAngle;
	private String txtCodebarType;

	public void buildUI( Composite parent )
	{
		if ( toolkit == null )
		{
			toolkit = new FormToolkit( Display.getCurrent( ) );
			toolkit.setBorderStyle( SWT.NULL );
		}

		Control[] children = parent.getChildren( );

		if ( children != null && children.length > 0 )
		{
			contentpane = (Composite) children[children.length - 1];

			GridLayout layout = new GridLayout( 3, false );
			layout.marginLeft = 8;
			layout.verticalSpacing = 12;
			contentpane.setLayout( layout );

			toolkit.createLabel( contentpane, "Content:" ); //$NON-NLS-1$
			txtText = toolkit. createText( contentpane, "", SWT.WRAP | SWT.MULTI| SWT.BORDER| SWT.H_SCROLL| SWT.V_SCROLL ); //$NON-NLS-1$
			GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
			gd.grabExcessVerticalSpace = true;
			gd.widthHint = 200;

			// XXX comment for expression support
			// gd.horizontalSpan = 2;

			txtText.setLayoutData( gd );
			/*TODO: Fix the problem with carriage return
			 * Got problem with carriage return, not able to solve it, YET
			 * txtText.addKeyListener(new KeyListener() {

				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					System.out.println("caret pressed>" + txtText.getCaretPosition() );
					updateModel( BarInCodesItem.CONTENT_PROP );
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					//updateModel( BarInCodesItem.CONTENT_PROP );
				
					// System.out.println("cursor UP >" + txtText.getCursor().toString() );
					System.out.println("caret >" + txtText.getCaretPosition() );
				}
				
			});*/
			txtText.addFocusListener( new FocusAdapter( ) {

				public void focusLost( org.eclipse.swt.events.FocusEvent e )
				{
					updateModel( BarInCodesItem.CONTENT_PROP );
				};
			} );

			// XXX uncomment this block for expression support
			// /*
			Button btnExp = toolkit.createButton( contentpane, "...", SWT.PUSH ); //$NON-NLS-1$
			btnExp.setToolTipText( "Invoke Expression Builder" ); //$NON-NLS-1$
			btnExp.addSelectionListener( new SelectionAdapter( ) {

				public void widgetSelected( SelectionEvent e )
				{
					openExpression( txtText );
				}
			} );
			// */

			toolkit.createLabel( contentpane, "Codebar type:" ); //$NON-NLS-1$
			cbCodeBarTypeList = getItem().getComboSelectList(contentpane);
			gd = new GridData( );
			gd.widthHint = 200;
			gd.horizontalSpan = 2;
			cbCodeBarTypeList.setLayoutData( gd );
			cbCodeBarTypeList.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					txtCodebarType = cbCodeBarTypeList.getText();
					updateModel( BarInCodesItem.TYPE_PROP );
				}
			});					    


		}
	}

	private void openExpression( Text textControl )
	{
		BarInCodesItem item = getItem();

		if ( item != null )
		{
			String oldValue = textControl.getText( );

			ExpressionBuilder eb = new ExpressionBuilder( textControl.getShell( ),
					oldValue );
			eb.setExpressionProvier( new ExpressionProvider( item.getModelHandle( ) ) );

			String result = oldValue;

			if ( eb.open( ) == Window.OK )
			{
				result = eb.getResult( );
			}

			if ( !oldValue.equals( result ) )
			{
				textControl.setText( result );

				updateModel( BarInCodesItem.CONTENT_PROP );
			}
		}
	}

	public void setInput( Object input )
	{
		this.input = input;
	}

	public void dispose( )
	{
		if ( toolkit != null )
		{
			toolkit.dispose( );
		}
	}

	private void adaptFormStyle( Composite comp )
	{
		Control[] children = comp.getChildren( );
		for ( int i = 0; i < children.length; i++ )
		{
			if ( children[i] instanceof Composite )
			{
				adaptFormStyle( (Composite) children[i] );
			}
		}

		toolkit.paintBordersFor( comp );
		toolkit.adapt( comp );
	}

	protected BarInCodesItem getItem( )
	{
		Object element = input;

		if ( input instanceof List && ( (List) input ).size( ) > 0 )
		{
			element = ( (List) input ).get( 0 );
		}

		if ( element instanceof ExtendedItemHandle )
		{
			try
			{
				return (BarInCodesItem) ( (ExtendedItemHandle) element ).getReportItem( );
			}
			catch ( Exception e )
			{
				e.printStackTrace( );
			}
		}

		return null;
	}

	public void refresh( )
	{
		if ( contentpane != null && !contentpane.isDisposed( ) )
		{
			if ( toolkit == null )
			{
				toolkit = new FormToolkit( Display.getCurrent( ) );
				toolkit.setBorderStyle( SWT.NULL );
			}

			adaptFormStyle( contentpane );

			updateUI( );
		}
	}

	public void postElementEvent( )
	{
		if ( contentpane != null && !contentpane.isDisposed( ) )
		{
			updateUI( );
		}
	}

	private void updateModel( String prop )
	{
		BarInCodesItem item = getItem( );

		if ( item != null )
		{
			try
			{
				if ( BarInCodesItem.ROTATION_ANGLE_PROP.equals( prop ) )
				{
					item.setRotationAngle( Integer.parseInt( txtAngle.getText( ) ));
				}
				else if ( BarInCodesItem.TYPE_PROP.equals( prop ) )
				{
					item.setType( txtCodebarType );
				}
				else if(BarInCodesItem.CONTENT_PROP.equals( prop ))
				{
					item.setContent( txtText.getText());
				}
				
			}
			catch ( Exception e )
			{
				e.printStackTrace( );
			}
		}
	}

	protected void updateUI( )
	{
		BarInCodesItem item = getItem( );

		if ( item != null )
		{
			String text = item.getContent( );
			txtText.setText( text == null ? "" : text ); //$NON-NLS-1$
			cbCodeBarTypeList.setText( String.valueOf( item.getType()) );
		}
	}
}
