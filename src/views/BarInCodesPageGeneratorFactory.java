package views;

import org.eclipse.birt.report.designer.ui.views.IPageGenerator;
import org.eclipse.core.runtime.IAdapterFactory;

public class BarInCodesPageGeneratorFactory implements IAdapterFactory {
	
	public Object getAdapter( Object adaptableObject, Class adapterType )
	{
		return new BarInCodesPageGenerator( );
	}

	public Class[] getAdapterList( )
	{
		return new Class[]{
			IPageGenerator.class
		};
	}
	
}
